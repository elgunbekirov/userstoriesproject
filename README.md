
1. http://localhost:8080/token/getToken       POST

body raw data 

{
	"username":"admin",
	"password":"password"
}

output  

{
    "userId": "admin",
    "accessToken": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInNjb3BlcyI6IlJPTEVfQURNSU4iLCJpYXQiOjE1OTM2NzA5OTYsImV4cCI6MTU5MzY4ODk5Nn0.t_H8mJTw3SGcf6ZiQc22sDc5PlPQc-jfsBCiQcYttqk"
}
----------------------------------------------

2. http://localhost:8080/credit/report/prepare     POST

POST

body raw data

{
	"userId": "4",
	"requestDebtAmount": "13000",
	"percent": "17",
	"loanPeriod":"24",
	"startDate":"2020-01-26T13:28:37.242Z",
	"customerWorkAge" : 95
}

Header 

Content-Tpe     application/json

Authorization   
Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInNjb3BlcyI6IlJPTEVfQURNSU4iLCJpYXQiOjE1OTM1NTMzNjcsImV4cCI6MTU5MzU3MTM2N30.w0Moj8tZjhxTOZy0z99rg7_WSYPZfN_rez5uX1rZLc4

-------------------------------------------------------
3. http://localhost:8080/bank/users        GET

header 

Authorization  

Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInNjb3BlcyI6IlJPTEVfQURNSU4iLCJpYXQiOjE1OTM1NTMzNjcsImV4cCI6MTU5MzU3MTM2N30.w0Moj8tZjhxTOZy0z99rg7_WSYPZfN_rez5uX1rZLc4


-------------------------------------------------------
4. http://localhost:8080/bank/userRequestList     GET

header 

Authorization  

Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInNjb3BlcyI6IlJPTEVfQURNSU4iLCJpYXQiOjE1OTM1NTMzNjcsImV4cCI6MTU5MzU3MTM2N30.w0Moj8tZjhxTOZy0z99rg7_WSYPZfN_rez5uX1rZLc4


-------------------------------------------------------
5. http://localhost:8080/bank/userResponseList      GET

header 

Authorization  

Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInNjb3BlcyI6IlJPTEVfQURNSU4iLCJpYXQiOjE1OTM1NTMzNjcsImV4cCI6MTU5MzU3MTM2N30.w0Moj8tZjhxTOZy0z99rg7_WSYPZfN_rez5uX1rZLc4

