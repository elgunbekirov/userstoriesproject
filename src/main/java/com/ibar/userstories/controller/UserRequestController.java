package com.ibar.userstories.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ibar.userstories.service.model.bean.ChartItem;
import com.ibar.userstories.service.model.bean.UserRequestItem;
import com.ibar.userstories.service.model.entity.User;
import com.ibar.userstories.service.model.entity.UserRequest;
import com.ibar.userstories.util.impl.UserInterface;
import com.ibar.userstories.util.impl.UserRequestInterface;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/bank")
public class UserRequestController {

    @Autowired
    private UserInterface userService;
	
    @Autowired
    private UserRequestInterface userRequestService;

   // @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value="/userRequestList", method = RequestMethod.GET)
    public List<UserRequest> listUserRequest(){
        return userRequestService.findAll();
    }
    
    @RequestMapping(value="/userRequestList2", method = RequestMethod.GET)
    public List<UserRequestItem> listUserRequest2(){
    	List<UserRequestItem> li = new ArrayList<UserRequestItem>();
    	List<UserRequest> usrReq = userRequestService.findAll();
    	for (UserRequest userRequest : usrReq) {
    		User u = userService.findById(userRequest.getUserId());
    		UserRequestItem uri = new UserRequestItem(u.getId(),
		    				                          u.getUsername(), u.getFullname(),
		    				                          userRequest.getGrantLoan(),
		    				                          userRequest.getRequestNumber(), 
		    				                          userRequest.getDescription(), 
		    				                          userRequest.getInDate());
    		li.add(uri);
		}
    	
      return li;
    }
    
    @RequestMapping(value="/userRequestListChart", method = RequestMethod.GET)
    public List<ChartItem> userRequestListChart(){
    	List<ChartItem> li = new ArrayList<ChartItem>();
    	List<UserRequest> usrReq = userRequestService.findAll();
    	for (UserRequest userRequest : usrReq) {
    		User u = userService.findById(userRequest.getUserId());
    		ChartItem uri = new ChartItem(u.getFullname(), String.valueOf(userRequest.getGrantLoan()));
    		li.add(uri);
		}    	
      return li;
    }
    

}
