package com.ibar.userstories.controller.model;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OutputResItem {
	
	private int numberOfMonth; 
	private BigDecimal willPercentCalcAmount;
	private BigDecimal monthlyDebtAmount;
	private BigDecimal percentAmount;
	private BigDecimal mainAmount;
	private BigDecimal totalRemainDebt;
	private ZonedDateTime paymentDate;
	
	public OutputResItem(ZonedDateTime paymentDate, BigDecimal willPercentCalcAmount, BigDecimal monthlyDebtAmount, BigDecimal percentAmount, BigDecimal mainAmount, BigDecimal totalRemainDebt) {
		this.paymentDate = paymentDate;
		this.willPercentCalcAmount = willPercentCalcAmount;
		this.monthlyDebtAmount = monthlyDebtAmount;
		this.percentAmount = percentAmount;
		this.mainAmount = mainAmount;
		this.totalRemainDebt = totalRemainDebt;
	}
	
	public int getNumberOfMonth() {
		return numberOfMonth;
	}
	
	public void setNumberOfMonth(int numberOfMonth) {
		this.numberOfMonth = numberOfMonth;
	}
	
	public BigDecimal getWillPercentCalcAmount() {
		return willPercentCalcAmount;
	}
	
	public void setWillPercentCalcAmount(BigDecimal willPercentCalcAmount) {
		this.willPercentCalcAmount = willPercentCalcAmount;
	}
	
	public BigDecimal getMonthlyDebtAmount() {
		return monthlyDebtAmount;
	}
	
	public void setMonthlyDebtAmount(BigDecimal monthlyDebtAmount) {
		this.monthlyDebtAmount = monthlyDebtAmount;
	}
	
	public BigDecimal getPercentAmount() {
		return percentAmount;
	}
	
	public void setPercentAmount(BigDecimal percentAmount) {
		this.percentAmount = percentAmount;
	}
	
	public BigDecimal getMainAmount() {
		return mainAmount;
	}
	
	public void setMainAmount(BigDecimal mainAmount) {
		this.mainAmount = mainAmount;
	}
	
	public ZonedDateTime getPaymentDate() {
		return paymentDate;
	}
	
	public void setPaymentDate(ZonedDateTime paymentDate) {
		this.paymentDate = paymentDate;
	}
	
	public BigDecimal getTotalRemainDebt() {
		return totalRemainDebt;
	}
	
	public void setTotalRemainDebt(BigDecimal totalRemainDebt) {
		this.totalRemainDebt = totalRemainDebt;
	}
}
