 package com.ibar.userstories.controller.model;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

public class InputReq {
    
	private int userId;
	private BigDecimal requestDebtAmount;
    private BigDecimal percent;
    private int loanPeriod;
    private ZonedDateTime startDate;
    private int customerWorkAge;
    
	public int getCustomerWorkAge() {
		return customerWorkAge;
	}

	public void setCustomerWorkAge(int customerWorkAge) {
		this.customerWorkAge = customerWorkAge;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public BigDecimal getRequestDebtAmount() {
		return requestDebtAmount;
	}

	public void setRequestDebtAmount(BigDecimal requestDebtAmount) {
		this.requestDebtAmount = requestDebtAmount;
	}

	public BigDecimal getPercent() {
		return percent;
	}

	public void setPercent(BigDecimal percent) {
		this.percent = percent;
	}

	public int getLoanPeriod() {
		return loanPeriod;
	}

	public void setLoanPeriod(int loanPeriod) {
		this.loanPeriod = loanPeriod;
	}

	public ZonedDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(ZonedDateTime startDate) {
		this.startDate = startDate;
	}
    
}
