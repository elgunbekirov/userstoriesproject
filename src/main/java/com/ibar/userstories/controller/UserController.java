package com.ibar.userstories.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ibar.userstories.service.model.bean.UserItem;
import com.ibar.userstories.service.model.entity.User;
import com.ibar.userstories.util.impl.UserInterface;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/bank")
public class UserController {

    @Autowired
    private UserInterface userService;

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value="/users", method = RequestMethod.GET)
    public List<User> listUser(){
        return userService.findAll();
    }

    @PreAuthorize("hasRole('USER')")
    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public User getOne(@PathVariable(value = "id") Long id){
        return userService.findById(id);
    }

    @RequestMapping(value="/save", method = RequestMethod.POST)
    public User saveUser(@RequestBody User user){
        return userService.save(user);
    }

    @RequestMapping(value="/deleteUser", method = RequestMethod.POST)
    public Long deleteUser(@RequestBody User user){
    	long userId = user.getId();
    	if(userId == 4) return new Long(-1);
    	userService.delete(userId);
        return userId;
    }
    
    @RequestMapping(value="/userList2", method = RequestMethod.GET)
    public List<UserItem> userList2(){
    	List<UserItem> li = new ArrayList<UserItem>();
    	List<User> usrList = userService.findAll();
    	for (User user : usrList) {
    		UserItem uri = new UserItem();
    		uri.setAddress(user.getAddress());
    		uri.setAge(user.getAge());    		
    		uri.setCreditamount(user.getCreditamount());
    		uri.setEmail(user.getEmail());
    		uri.setFullname(user.getFullname());
    		uri.setId(user.getId());
    		uri.setLoanperiod(user.getLoanperiod());
    		uri.setPassword(user.getPassword());
    		uri.setPercent(user.getPercent());
    		uri.setPhone(user.getPhone());
    		uri.setSalary(user.getSalary());
    		uri.setUsername(user.getUsername());
    		uri.setWorkage(user.getWorkage());
    	  if(!user.getUsername().equals("admin")) li.add(uri);
		}
    	
      return li;
    }

}
