package com.ibar.userstories.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ibar.userstories.service.model.bean.SumResponse;
import com.ibar.userstories.service.model.bean.UserResponseItem;
import com.ibar.userstories.service.model.entity.UserResponse;
import com.ibar.userstories.util.impl.UserResponseInterface;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/bank")
public class UserResponseController {
	
    @Autowired
    private UserResponseInterface usrResponseService;

   // @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value="/userResponseList", method = RequestMethod.GET)
    public List<UserResponse> listUserResponse(){
        return usrResponseService.findAll();
    }
    
    @RequestMapping(value="/userResponseList2", method = RequestMethod.GET)
    public List<UserResponseItem> listUserResponse2(){
    	List<UserResponseItem> li = new ArrayList<UserResponseItem>();
    	List<UserResponse> usrRes = usrResponseService.findAll();
    	for (UserResponse userResponse : usrRes) {
    		UserResponseItem uri = new UserResponseItem(userResponse.getUserRequestId(), 
    				userResponse.getNumberOfMonth(), userResponse.getPaymentDate(), 
    				userResponse.getWillPercentCalcAmount(), userResponse.getMonthlyDebtAmount(), 
    				userResponse.getPercentAmount(), userResponse.getMainAmount(), userResponse.getTotalRemainDebt());
    		li.add(uri);
		}
    	
        return li;
    }
    
    @RequestMapping(value="/listUserResponseSum", method = RequestMethod.GET)
    public List<SumResponse>  listUserResponseSum(){
    	List<SumResponse> res = new ArrayList<>();
    	List<UserResponse> usrRes = usrResponseService.findAll();
    	BigDecimal willPercentCalcAmount = new BigDecimal("0");    	
    	BigDecimal monthlyDebtAmount = new BigDecimal("0");
    	BigDecimal percentAmount = new BigDecimal("0");
    	BigDecimal mainAmount = new BigDecimal("0");
    	BigDecimal totalRemainDebt = new BigDecimal("0");
    	
    	for (UserResponse userResponse : usrRes) {
    		willPercentCalcAmount = willPercentCalcAmount.add(userResponse.getWillPercentCalcAmount());
    		monthlyDebtAmount = monthlyDebtAmount.add(userResponse.getMonthlyDebtAmount());
    		percentAmount = percentAmount.add(userResponse.getPercentAmount());
    		mainAmount = mainAmount.add(userResponse.getMainAmount());
    		totalRemainDebt = totalRemainDebt.add(userResponse.getTotalRemainDebt());
		}    	
    	SumResponse uri = new SumResponse(willPercentCalcAmount.toString(),
    			                          monthlyDebtAmount.toString(), 
    			                          percentAmount.toString(),
    			                          mainAmount.toString(), 
    			                          totalRemainDebt.toString());  
    	res.add(uri);
        return res;
    }

}
