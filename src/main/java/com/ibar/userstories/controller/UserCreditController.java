package com.ibar.userstories.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ibar.userstories.controller.model.InputReq;
import com.ibar.userstories.controller.model.OutputResItem;
import com.ibar.userstories.service.ReportPrepareServiceImpl;
import com.ibar.userstories.service.model.bean.UserResponseItem;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/credit/report")
public class UserCreditController {

	@Autowired
	private ReportPrepareServiceImpl calculatedLoanList;

    @PostMapping("prepare")
    public List<OutputResItem> prepareReport(@RequestBody @Valid InputReq request) {
    	List<OutputResItem> res = calculatedLoanList.prepareReportStory(request.getUserId(), request.getCustomerWorkAge(), request.getRequestDebtAmount(),
    			                                  request.getPercent(), request.getLoanPeriod(), request.getStartDate())
                .stream()
                .map(this::convertToOutputItem)
                .collect(Collectors.toList());    	
    	return res;
    }

    private OutputResItem convertToOutputItem(UserResponseItem storyItem) {
        return new OutputResItem(storyItem.getPaymentDate(), 
        		                    storyItem.getWillPercentCalcAmount(), 
        		                    storyItem.getMonthlyDebtAmount(), 
        		                    storyItem.getPercentAmount(),
        		                    storyItem.getMainAmount(),
        		                    storyItem.getTotalRemainDebt());
    }    
    
}
