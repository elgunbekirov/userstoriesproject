package com.ibar.userstories.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ibar.userstories.service.model.entity.UserResponse;

@Repository
public interface UserResponseRepository extends CrudRepository<UserResponse, Long> { 
}
