package com.ibar.userstories.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ibar.userstories.service.model.entity.UserRequest;

@Repository
public interface UserRequestRepository extends CrudRepository<UserRequest, Long> {  
}
