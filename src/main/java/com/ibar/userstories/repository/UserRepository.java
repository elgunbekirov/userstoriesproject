package com.ibar.userstories.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ibar.userstories.service.model.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {    
	User findByUsername(String username);    
}
