package com.ibar.userstories.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibar.userstories.service.model.bean.UserResponseItem;
import com.ibar.userstories.service.model.entity.UserRequest;
import com.ibar.userstories.service.model.entity.UserResponse;
import com.ibar.userstories.util.Utils;
import com.ibar.userstories.util.impl.PrepareReportInterface;


@Service(value = "reportService")
public class ReportPrepareServiceImpl {
	
    private static int sizeOfMonthInYear = 12;

    @Autowired
    private PrepareReportInterface reportPrepareServiceImpl;
    
    @Autowired
    private UserRequestServiceImpl userRequestServiceImpl;
    
    @Autowired
    private UserResponseServiceImpl userResponseServiceImpl;
    
    public List<UserResponseItem> prepareReportStory(int userId, int customerWorkAge, BigDecimal principalBalance, BigDecimal annualInterestInPercent, int duration, ZonedDateTime startDate) {

        BigDecimal annualInterestRate = convertInterestPercentToRate(annualInterestInPercent);

        BigDecimal monthlyInterestRate = convertAnnualInterestRateToMonthly(annualInterestRate);

        BigDecimal annuity = reportPrepareServiceImpl.calculateCredit(principalBalance, monthlyInterestRate, duration);

        UserRequest userRequest = saveUserRequest(userId, customerWorkAge, principalBalance.intValue());
        
        List<UserResponseItem> planItems = new ArrayList<>();
        
        for (int month = 0; month < duration; month++) {
           
        	UserResponseItem planItem = prepareMonthlyLoan(Integer.valueOf(userRequest.getRequestNumber()), Integer.valueOf(month+1), startDate, principalBalance, annuity, monthlyInterestRate);

            planItems.add(planItem);

            if(userRequest.getGrantLoan() > 0)
              saveUserResponse(planItem);
            
            principalBalance = planItem.getTotalRemainDebt();
        }
        
        return planItems;
    }
    
    private UserResponse saveUserResponse(UserResponseItem responseItem) {
 		UserResponse item = new UserResponse(); 
 		item.setMainAmount(responseItem.getMainAmount());
 		item.setMonthlyDebtAmount(responseItem.getMonthlyDebtAmount());
 		item.setNumberOfMonth(responseItem.getNumberOfMonth());
 		item.setPaymentDate(responseItem.getPaymentDate());
 		item.setPercentAmount(responseItem.getPercentAmount());
 		item.setTotalRemainDebt(responseItem.getTotalRemainDebt());
 		item.setUserRequestId(responseItem.getUserRequestId());
 		item.setWillPercentCalcAmount(responseItem.getWillPercentCalcAmount());
 		userResponseServiceImpl.save(item);     	
       return item;
     }
    
    private UserRequest saveUserRequest(int userId, int customerWorkAge, int grantLoan) {
    	String requestNumber = Utils.generateAvtoNumber();
    	 Map<String, String> riskCheckMap = getCreditRiskResult(customerWorkAge);
    		UserRequest item = new UserRequest(); 
    		item.setUserId(userId);
    		item.setRequestNumber(Integer.parseInt(requestNumber));
    		item.setGrantLoan(Integer.parseInt(riskCheckMap.get("grantLoan")));
    		item.setDescription(riskCheckMap.get("description"));
    		item.setInDate(ZonedDateTime.now());    		
    		item.setGrantLoan(Integer.parseInt(riskCheckMap.get("grantLoan")) > 0 ? grantLoan : 0);
    		userRequestServiceImpl.save(item);
        return item;
    }
    
    private Map<String, String> getCreditRiskResult(int customerWorkAge){
    	Map<String, String> res = new HashMap<>();
    	if(customerWorkAge >= 90) {
    	 res.put("grantLoan", "1");
    	 res.put("description", "Kredit xətti müvəffəqiyyətlə ayrıldı");
    	}
    	else
    	{
    	  res.put("grantLoan", "0");
       	  res.put("description", "Təəsüff.Risk meyyari olduğu üçün Kredit ayrılmadı");
    	}
      return res;
    }

    private UserResponseItem prepareMonthlyLoan(Integer userRequestId, Integer month, ZonedDateTime startDate, BigDecimal principalBalance, BigDecimal annuity, BigDecimal monthlyInterestRate) {
    	
        BigDecimal paidInterest = reportPrepareServiceImpl.percentAmountCalculate(principalBalance, monthlyInterestRate);
        
        BigDecimal paidPrincipal = reportPrepareServiceImpl.mainAmountCalculate(annuity, paidInterest);

        if (paidPrincipal.compareTo(principalBalance) > 0) {
        	
            paidPrincipal = principalBalance;

            annuity = recalculateAnnuity(paidInterest, paidPrincipal);
        }

        BigDecimal newPrincipalBalance = principalBalance.subtract(paidPrincipal);

        return new UserResponseItem(userRequestId, month,  startDate.plusMonths(month.intValue()), principalBalance, annuity, paidInterest, paidPrincipal, newPrincipalBalance);
    }

    private BigDecimal convertInterestPercentToRate(BigDecimal interestInPercent) {
        return interestInPercent.divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_EVEN);
    }

    private BigDecimal convertAnnualInterestRateToMonthly(BigDecimal annualInterestRate) {
        return annualInterestRate.divide(BigDecimal.valueOf(sizeOfMonthInYear), 8, RoundingMode.HALF_EVEN);
    }

    private BigDecimal recalculateAnnuity(BigDecimal paidInterest, BigDecimal paidPrincipal) {
        return paidInterest.add(paidPrincipal);
    }
}
