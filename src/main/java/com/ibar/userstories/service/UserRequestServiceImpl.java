package com.ibar.userstories.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibar.userstories.repository.UserRequestRepository;
import com.ibar.userstories.service.model.entity.UserRequest;
import com.ibar.userstories.util.impl.UserRequestInterface;


@Service(value = "userRequestServiceImpl")
public class UserRequestServiceImpl implements UserRequestInterface {
	
	@Autowired
	private UserRequestRepository userRequestServiceImpl;

	public List<UserRequest> findAll() {
		List<UserRequest> list = new ArrayList<>();
		userRequestServiceImpl.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
    public UserRequest save(UserRequest item) {
        return userRequestServiceImpl.save(item);
    }

	@Override
	public void delete(long id) {
		userRequestServiceImpl.deleteById(id);
	}

	@Override
	public UserRequest findOne(String username) {
		return null;
	}

	@Override
	public UserRequest findById(Long id) {
		return null;
	}
	
}
