package com.ibar.userstories.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibar.userstories.repository.UserResponseRepository;
import com.ibar.userstories.service.model.entity.UserResponse;
import com.ibar.userstories.util.impl.UserResponseInterface;


@Service(value = "userResponseServiceImpl")
public class UserResponseServiceImpl implements UserResponseInterface {
	
	@Autowired
	private UserResponseRepository userResponseServiceImpl;

	public List<UserResponse> findAll() {
		List<UserResponse> list = new ArrayList<>();
		userResponseServiceImpl.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
	public void delete(long id) {
		userResponseServiceImpl.deleteById(id);
	}


	@Override
    public UserResponse save(UserResponse item) {
        return userResponseServiceImpl.save(item);
    }

	@Override
	public UserResponse findOne(String item) {
		return null;
	}

	@Override
	public UserResponse findById(Long id) {
		return null;
	}

}
