package com.ibar.userstories.service.model.entity;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class UserResponse {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    
    @Column(name="user_request_id")
    private int userRequestId;
    
    @Column(name="number_of_month")
    private int numberOfMonth;
    
    @Column(name="will_percent_calc_amount")
    private BigDecimal willPercentCalcAmount;

    @Column(name="monthly_debt_amount")
    private BigDecimal monthlyDebtAmount;
    
    @Column(name="percent_amount")
    private BigDecimal percentAmount;
    
    @Column(name="main_amount")
    private BigDecimal mainAmount;
    
    @Column(name="total_remain_debt")
    private BigDecimal totalRemainDebt;
    
    @Column(name="payment_date")
    private ZonedDateTime paymentDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserRequestId() {
		return userRequestId;
	}

	public void setUserRequestId(int userRequestId) {
		this.userRequestId = userRequestId;
	}

	public int getNumberOfMonth() {
		return numberOfMonth;
	}

	public void setNumberOfMonth(int numberOfMonth) {
		this.numberOfMonth = numberOfMonth;
	}

	public BigDecimal getWillPercentCalcAmount() {
		return willPercentCalcAmount;
	}

	public void setWillPercentCalcAmount(BigDecimal willPercentCalcAmount) {
		this.willPercentCalcAmount = willPercentCalcAmount;
	}

	public BigDecimal getMonthlyDebtAmount() {
		return monthlyDebtAmount;
	}

	public void setMonthlyDebtAmount(BigDecimal monthlyDebtAmount) {
		this.monthlyDebtAmount = monthlyDebtAmount;
	}

	public BigDecimal getPercentAmount() {
		return percentAmount;
	}

	public void setPercentAmount(BigDecimal percentAmount) {
		this.percentAmount = percentAmount;
	}

	public BigDecimal getMainAmount() {
		return mainAmount;
	}

	public void setMainAmount(BigDecimal mainAmount) {
		this.mainAmount = mainAmount;
	}

	public BigDecimal getTotalRemainDebt() {
		return totalRemainDebt;
	}

	public void setTotalRemainDebt(BigDecimal totalRemainDebt) {
		this.totalRemainDebt = totalRemainDebt;
	}

	public ZonedDateTime getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(ZonedDateTime paymentDate) {
		this.paymentDate = paymentDate;
	}
    
}
