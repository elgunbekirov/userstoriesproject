package com.ibar.userstories.service.model.bean;

public class UserItem {

    private long id;

    private String username;
    
    private String password;
    
    private String fullname;
    
    private long salary;
    
    private int age;
    
    private int workage;
    
    private String email;

    private String phone;
    
    private String address;
    
    private String percent;
    
    private String loanperiod;
    
    private String creditamount;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public long getSalary() {
		return salary;
	}

	public void setSalary(long salary) {
		this.salary = salary;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getWorkage() {
		return workage;
	}

	public void setWorkage(int workage) {
		this.workage = workage;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPercent() {
		return percent;
	}

	public void setPercent(String percent) {
		this.percent = percent;
	}

	public String getLoanperiod() {
		return loanperiod;
	}

	public void setLoanperiod(String loanperiod) {
		this.loanperiod = loanperiod;
	}

	public String getCreditamount() {
		return creditamount;
	}

	public void setCreditamount(String creditamount) {
		this.creditamount = creditamount;
	}

    
}
