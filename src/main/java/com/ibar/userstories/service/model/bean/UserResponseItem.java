package com.ibar.userstories.service.model.bean;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

public class UserResponseItem {
	
	private Integer userRequestId;
    private Integer numberOfMonth; 
    private BigDecimal willPercentCalcAmount;
    private BigDecimal monthlyDebtAmount;
    private BigDecimal percentAmount;
    private BigDecimal mainAmount;
    private BigDecimal totalRemainDebt;
    private ZonedDateTime paymentDate;
    
	public UserResponseItem(Integer userRequestId, Integer numberOfMonth, ZonedDateTime paymentDate, 
								BigDecimal willPercentCalcAmount, BigDecimal monthlyDebtAmount,
								BigDecimal percentAmount, BigDecimal mainAmount, BigDecimal totalRemainDebt) {		
		this.userRequestId = userRequestId;
		this.numberOfMonth = numberOfMonth;
		this.paymentDate = paymentDate;
		this.willPercentCalcAmount = willPercentCalcAmount;
		this.monthlyDebtAmount = monthlyDebtAmount;
		this.percentAmount = percentAmount;
		this.mainAmount = mainAmount;
		this.totalRemainDebt = totalRemainDebt;
	}

	public Integer getUserRequestId() {
		return userRequestId;
	}

	public void setUserRequestId(Integer userRequestId) {
		this.userRequestId = userRequestId;
	}

	public Integer getNumberOfMonth() {
		return numberOfMonth;
	}

	public void setNumberOfMonth(Integer numberOfMonth) {
		this.numberOfMonth = numberOfMonth;
	}

	public BigDecimal getWillPercentCalcAmount() {
		return willPercentCalcAmount;
	}

	public void setWillPercentCalcAmount(BigDecimal willPercentCalcAmount) {
		this.willPercentCalcAmount = willPercentCalcAmount;
	}

	public BigDecimal getMonthlyDebtAmount() {
		return monthlyDebtAmount;
	}

	public void setMonthlyDebtAmount(BigDecimal monthlyDebtAmount) {
		this.monthlyDebtAmount = monthlyDebtAmount;
	}

	public BigDecimal getPercentAmount() {
		return percentAmount;
	}

	public void setPercentAmount(BigDecimal percentAmount) {
		this.percentAmount = percentAmount;
	}

	public BigDecimal getMainAmount() {
		return mainAmount;
	}

	public void setMainAmount(BigDecimal mainAmount) {
		this.mainAmount = mainAmount;
	}

	public BigDecimal getTotalRemainDebt() {
		return totalRemainDebt;
	}

	public void setTotalRemainDebt(BigDecimal totalRemainDebt) {
		this.totalRemainDebt = totalRemainDebt;
	}

	public ZonedDateTime getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(ZonedDateTime paymentDate) {
		this.paymentDate = paymentDate;
	}
	
}
