package com.ibar.userstories.service.model.bean;

public class AuthToken {

    private String userId;
    private String accessToken;

    public AuthToken(String accessToken, String userId){
        this.accessToken = accessToken;
        this.userId = userId;
    }

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
