package com.ibar.userstories.service.model.entity;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class UserRequest {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    
    @Column(name="user_id")
    private long userId;
    
    @Column(name="request_number")
    private int requestNumber;
    
    @Column(name="description")
    private String description;

    @Column(name="in_date")
    private ZonedDateTime inDate;
    
    @Column(name="grant_loan")
    private int grantLoan;
    

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}


	public int getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(int requestNumber) {
		this.requestNumber = requestNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ZonedDateTime getInDate() {
		return inDate;
	}

	public void setInDate(ZonedDateTime inDate) {
		this.inDate = inDate;
	}

	public int getGrantLoan() {
		return grantLoan;
	}

	public void setGrantLoan(int grantLoan) {
		this.grantLoan = grantLoan;
	}
    

}
