package com.ibar.userstories.service.model.bean;

import java.time.ZonedDateTime;

public class UserRequestItem {
	
	private long userId;

	private String userName;
	
	private String fullName;
	
	private int requestNumber;
	
	private String description;
	
	private ZonedDateTime inDate;
	
	private int grantLoan;

	
	public UserRequestItem(long userId, String userName, String fullName, int grantLoan,  int requestNumber, String description, ZonedDateTime inDate) {
		this.userId = userId;

		this.userName = userName;
		
		this.requestNumber = requestNumber;
		
		this.description = description;
		
		this.inDate = inDate;
		
		this.fullName = fullName;
		
		this.grantLoan = grantLoan;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public int getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(int requestNumber) {
		this.requestNumber = requestNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ZonedDateTime getInDate() {
		return inDate;
	}

	public void setInDate(ZonedDateTime inDate) {
		this.inDate = inDate;
	}

	public int getGrantLoan() {
		return grantLoan;
	}

	public void setGrantLoan(int grantLoan) {
		this.grantLoan = grantLoan;
	}

	
	
}
