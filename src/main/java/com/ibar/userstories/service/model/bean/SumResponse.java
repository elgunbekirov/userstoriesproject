package com.ibar.userstories.service.model.bean;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SumResponse {

    private String sumWillPercentCalcAmount;
    private String sumMonthlyDebtAmount;
    private String sumPercentAmount;
    private String sumMainAmount;
    private String sumTotalRemainDebt;
    
    public SumResponse(String sumWillPercentCalcAmount, String sumMonthlyDebtAmount, String sumPercentAmount, String sumMainAmount, String sumTotalRemainDebt) {
        this.sumWillPercentCalcAmount = sumWillPercentCalcAmount;
        this.sumMonthlyDebtAmount = sumMonthlyDebtAmount;
        this.sumPercentAmount = sumPercentAmount;
        this.sumMainAmount = sumMainAmount;
        this.sumTotalRemainDebt = sumTotalRemainDebt;
    }

	public String getSumWillPercentCalcAmount() {
		return sumWillPercentCalcAmount;
	}

	public void setSumWillPercentCalcAmount(String sumWillPercentCalcAmount) {
		this.sumWillPercentCalcAmount = sumWillPercentCalcAmount;
	}

	public String getSumMonthlyDebtAmount() {
		return sumMonthlyDebtAmount;
	}

	public void setSumMonthlyDebtAmount(String sumMonthlyDebtAmount) {
		this.sumMonthlyDebtAmount = sumMonthlyDebtAmount;
	}

	public String getSumPercentAmount() {
		return sumPercentAmount;
	}

	public void setSumPercentAmount(String sumPercentAmount) {
		this.sumPercentAmount = sumPercentAmount;
	}

	public String getSumMainAmount() {
		return sumMainAmount;
	}

	public void setSumMainAmount(String sumMainAmount) {
		this.sumMainAmount = sumMainAmount;
	}

	public String getSumTotalRemainDebt() {
		return sumTotalRemainDebt;
	}

	public void setSumTotalRemainDebt(String sumTotalRemainDebt) {
		this.sumTotalRemainDebt = sumTotalRemainDebt;
	}

	
    
}
