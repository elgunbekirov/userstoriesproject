package com.ibar.userstories.service.model.bean;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ChartItem {

	String label;
    String value;
    
    public ChartItem(String label, String value) {
    	this.label = label;
    	this.value = value;
    }

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
    
    
    
}
