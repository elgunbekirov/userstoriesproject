package com.ibar.userstories.util.impl;

import java.util.List;

import com.ibar.userstories.service.model.entity.UserResponse;

public interface UserResponseInterface {

    UserResponse save(UserResponse item);
    List<UserResponse> findAll();
    void delete(long id);
    UserResponse findOne(String item);
    UserResponse findById(Long id);
 
}
