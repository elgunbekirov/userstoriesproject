package com.ibar.userstories.util.impl;

import java.util.List;

import com.ibar.userstories.service.model.entity.UserRequest;

public interface UserRequestInterface {

    UserRequest save(UserRequest item);
    List<UserRequest> findAll();
    void delete(long id);
    UserRequest findOne(String item);
    UserRequest findById(Long id);
 
}
