package com.ibar.userstories.util.impl;

import java.util.List;

import com.ibar.userstories.service.model.entity.User;

public interface UserInterface {

    User save(User item);
    List<User> findAll();
    void delete(long id);
    User findOne(String item);
    User findById(Long id);
    
}
