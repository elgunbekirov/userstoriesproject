package com.ibar.userstories.util.impl;

import java.math.BigDecimal;


public interface PrepareReportInterface {
	
    public BigDecimal percentAmountCalculate(BigDecimal principalBalance, BigDecimal interestRate);

    public BigDecimal mainAmountCalculate(BigDecimal annuityAmount, BigDecimal paidInterest);

    public BigDecimal calculateCredit(BigDecimal principalBalance, BigDecimal interestRate, int installmentCount);
        
}
