package com.ibar.userstories.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.stereotype.Component;

import com.ibar.userstories.util.impl.PrepareReportInterface;

@Component
public class MonthlyCreditCalc implements PrepareReportInterface {

    @Override
    public BigDecimal percentAmountCalculate(BigDecimal amount, BigDecimal interestRate) {
        return amount.multiply(interestRate).setScale(2, RoundingMode.HALF_EVEN);
    }

    @Override
    public BigDecimal mainAmountCalculate(BigDecimal amount, BigDecimal paidPercent) {
        return amount.subtract(paidPercent).setScale(2, RoundingMode.HALF_EVEN);
    }

    @Override
    public BigDecimal calculateCredit(BigDecimal amount, BigDecimal interestRate, int countOfMonth) {
        BigDecimal numerator = amount.multiply(interestRate);
        BigDecimal power = interestRate.add(BigDecimal.ONE).pow(countOfMonth).setScale(8, RoundingMode.HALF_EVEN);
        BigDecimal reverse = BigDecimal.ONE.divide(power, 8, RoundingMode.HALF_EVEN);
        BigDecimal denominator = BigDecimal.ONE.subtract(reverse);
        return numerator.divide(denominator, 2, RoundingMode.UP);
    }
}
