package com.ibar.userstories.util;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class Utils {

	public static String generateAvtoNumber() {
		SecureRandom prng;
		try {
			prng = SecureRandom.getInstance("SHA1PRNG");
			String randomNum = new Integer(prng.nextInt()).toString();
			int val = Math.abs(Integer.parseInt(randomNum));
			String res = String.valueOf(val);

			if (res.length() > 7)
				res = res.substring(0, 8);

			return res;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return "";
		}

	}
	
}
