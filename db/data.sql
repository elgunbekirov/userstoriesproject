/*
SQLyog - Free MySQL GUI v5.19
Host - 5.1.73-community : Database - user_stories_db
*********************************************************************
Server version : 5.1.73-community
*/

SET NAMES utf8;

SET SQL_MODE='';

create database if not exists `user_stories_db`;

USE `user_stories_db`;

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `role` */

insert into `role` (`id`,`description`,`name`) values (4,'Admin role','ADMIN'),(5,'User role','USER');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `age` int(11) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salary` bigint(20) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `fullname` varchar(300) DEFAULT NULL,
  `phone` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `workage` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `percent` int(11) DEFAULT '1',
  `loanperiod` int(11) DEFAULT '12',
  `creditamount` int(11) DEFAULT '0',
  `startdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert into `user` (`id`,`age`,`password`,`salary`,`username`,`fullname`,`phone`,`email`,`address`,`workage`,`status`,`percent`,`loanperiod`,`creditamount`,`startdate`) values (1,33,'$2a$04$Ye7/lJoJin6.m9sOJZ9ujeTgHEVM4VXgI2Ingpsnf9gXyXEXf/IlW',3456,'elgun.b','Elgun Bekirov','564131321','','Yasamal',90,1,15,12,8000,NULL),(2,23,'$2a$04$StghL1FYVyZLdi8/DIkAF./2rz61uiYPI3.MaAph5hUq03XKeflyW',7823,'emin.v','Elvin Veliyev','234234234','','Xirdalan',60,1,20,24,10000,NULL),(3,45,'$2a$04$Lk4zqXHrHd82w5/tiMy8ru9RpAXhvFfmHOuqTmFPWQcUhBD8SSJ6W',4234,'elcin.a','Elcin Almemmedov','32453425','','Nerimanov',96,1,22,36,5000,NULL),(4,85,'$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6',4234,'admin','Admin Adminov','34563456','','Sabuncu',110,1,24,6,16000,NULL),(5,50,'$2a$10$35vpn0OHaTnIObz/h.OakOL0DS61PzRqEqk/LWgPi0gDV7CMLmSea',13000,'ibar','Ibar Ibarovic','23453245','','Nizami',30,1,26,9,20000,NULL),(12,0,NULL,2000,'1dfc-c156-14d1-d805','wwwww','','','',100,1,15,24,1000,NULL);

/*Table structure for table `user_request` */

DROP TABLE IF EXISTS `user_request`;

CREATE TABLE `user_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `request_number` varchar(45) DEFAULT NULL,
  `description` varchar(4000) DEFAULT NULL,
  `in_date` varchar(45) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `grant_loan` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `user_request` */

insert into `user_request` (`id`,`user_id`,`request_number`,`description`,`in_date`,`status`,`grant_loan`) values (1,2,'12586609','Kredit xətti müvəffəqiyyətlə ayrıldı','2020-07-02 07:41:06',1,1),(2,2,'63775164','Təəsüff.Risk meyyari olduğu üçün Kredit ayrılmadı','2020-07-02 09:26:40',1,0),(3,12,'55123170','Kredit xətti müvəffəqiyyətlə ayrıldı','2020-07-02 10:11:20',1,1),(4,5,'17815146','Təəsüff.Risk meyyari olduğu üçün Kredit ayrılmadı','2020-07-02 10:12:15',1,0);

/*Table structure for table `user_response` */

DROP TABLE IF EXISTS `user_response`;

CREATE TABLE `user_response` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_request_id` int(11) DEFAULT NULL,
  `number_of_month` int(11) DEFAULT NULL,
  `will_percent_calc_amount` decimal(10,2) DEFAULT NULL,
  `monthly_debt_amount` decimal(10,2) DEFAULT NULL,
  `percent_amount` decimal(10,2) DEFAULT NULL,
  `main_amount` decimal(10,2) DEFAULT NULL,
  `total_remain_debt` decimal(10,2) DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COMMENT='    private int numberOfMonth; \n    private BigDecimal willP';

/*Data for the table `user_response` */

insert into `user_response` (`id`,`user_request_id`,`number_of_month`,`will_percent_calc_amount`,`monthly_debt_amount`,`percent_amount`,`main_amount`,`total_remain_debt`,`payment_date`,`status`) values (1,12586609,1,'13000.00','642.75','184.17','458.58','12541.42','2020-02-01 17:28:37',1),(2,12586609,2,'12541.42','642.75','177.67','465.08','12076.34','2020-03-01 17:28:37',1),(3,12586609,3,'12076.34','642.75','171.08','471.67','11604.67','2020-04-01 17:28:37',1),(4,12586609,4,'11604.67','642.75','164.40','478.35','11126.32','2020-05-01 17:28:37',1),(5,12586609,5,'11126.32','642.75','157.62','485.13','10641.19','2020-06-01 17:28:37',1),(6,12586609,6,'10641.19','642.75','150.75','492.00','10149.19','2020-07-01 17:28:37',1),(7,12586609,7,'10149.19','642.75','143.78','498.97','9650.22','2020-08-01 17:28:37',1),(8,12586609,8,'9650.22','642.75','136.71','506.04','9144.18','2020-09-01 17:28:37',1),(9,12586609,9,'9144.18','642.75','129.54','513.21','8630.97','2020-10-01 17:28:37',1),(10,12586609,10,'8630.97','642.75','122.27','520.48','8110.49','2020-11-01 17:28:37',1),(11,12586609,11,'8110.49','642.75','114.90','527.85','7582.64','2020-12-01 17:28:37',1),(12,12586609,12,'7582.64','642.75','107.42','535.33','7047.31','2021-01-01 17:28:37',1),(13,12586609,13,'7047.31','642.75','99.84','542.91','6504.40','2021-02-01 17:28:37',1),(14,12586609,14,'6504.40','642.75','92.15','550.60','5953.80','2021-03-01 17:28:37',1),(15,12586609,15,'5953.80','642.75','84.35','558.40','5395.40','2021-04-01 17:28:37',1),(16,12586609,16,'5395.40','642.75','76.43','566.32','4829.08','2021-05-01 17:28:37',1),(17,12586609,17,'4829.08','642.75','68.41','574.34','4254.74','2021-06-01 17:28:37',1),(18,12586609,18,'4254.74','642.75','60.28','582.47','3672.27','2021-07-01 17:28:37',1),(19,12586609,19,'3672.27','642.75','52.02','590.73','3081.54','2021-08-01 17:28:37',1),(20,12586609,20,'3081.54','642.75','43.66','599.09','2482.45','2021-09-01 17:28:37',1),(21,12586609,21,'2482.45','642.75','35.17','607.58','1874.87','2021-10-01 17:28:37',1),(22,12586609,22,'1874.87','642.75','26.56','616.19','1258.68','2021-11-01 17:28:37',1),(23,12586609,23,'1258.68','642.75','17.83','624.92','633.76','2021-12-01 17:28:37',1),(24,12586609,24,'633.76','642.74','8.98','633.76','0.00','2022-01-01 17:28:37',1),(25,55123170,1,'1000.00','48.49','12.50','35.99','964.01','2020-02-01 17:28:37',1),(26,55123170,2,'964.01','48.49','12.05','36.44','927.57','2020-03-01 17:28:37',1),(27,55123170,3,'927.57','48.49','11.59','36.90','890.67','2020-04-01 17:28:37',1),(28,55123170,4,'890.67','48.49','11.13','37.36','853.31','2020-05-01 17:28:37',1),(29,55123170,5,'853.31','48.49','10.67','37.82','815.49','2020-06-01 17:28:37',1),(30,55123170,6,'815.49','48.49','10.19','38.30','777.19','2020-07-01 17:28:37',1),(31,55123170,7,'777.19','48.49','9.71','38.78','738.41','2020-08-01 17:28:37',1),(32,55123170,8,'738.41','48.49','9.23','39.26','699.15','2020-09-01 17:28:37',1),(33,55123170,9,'699.15','48.49','8.74','39.75','659.40','2020-10-01 17:28:37',1),(34,55123170,10,'659.40','48.49','8.24','40.25','619.15','2020-11-01 17:28:37',1),(35,55123170,11,'619.15','48.49','7.74','40.75','578.40','2020-12-01 17:28:37',1),(36,55123170,12,'578.40','48.49','7.23','41.26','537.14','2021-01-01 17:28:37',1),(37,55123170,13,'537.14','48.49','6.71','41.78','495.36','2021-02-01 17:28:37',1),(38,55123170,14,'495.36','48.49','6.19','42.30','453.06','2021-03-01 17:28:37',1),(39,55123170,15,'453.06','48.49','5.66','42.83','410.23','2021-04-01 17:28:37',1),(40,55123170,16,'410.23','48.49','5.13','43.36','366.87','2021-05-01 17:28:37',1),(41,55123170,17,'366.87','48.49','4.59','43.90','322.97','2021-06-01 17:28:37',1),(42,55123170,18,'322.97','48.49','4.04','44.45','278.52','2021-07-01 17:28:37',1),(43,55123170,19,'278.52','48.49','3.48','45.01','233.51','2021-08-01 17:28:37',1),(44,55123170,20,'233.51','48.49','2.92','45.57','187.94','2021-09-01 17:28:37',1),(45,55123170,21,'187.94','48.49','2.35','46.14','141.80','2021-10-01 17:28:37',1),(46,55123170,22,'141.80','48.49','1.77','46.72','95.08','2021-11-01 17:28:37',1),(47,55123170,23,'95.08','48.49','1.19','47.30','47.78','2021-12-01 17:28:37',1),(48,55123170,24,'47.78','48.38','0.60','47.78','0.00','2022-01-01 17:28:37',1);

/*Table structure for table `user_roles` */

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FKrhfovtciq1l558cw6udg0h0d3` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `user_roles` */

insert into `user_roles` (`user_id`,`role_id`) values (1,4),(2,5),(4,4);

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
